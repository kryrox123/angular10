import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
@Component({
  selector: 'app-cuerpo',
  templateUrl: './cuerpo.component.html',
  styleUrls: ['./cuerpo.component.css']
})


export class CuerpoComponent implements OnInit {

  cuestionario!: FormGroup;
  pre1: string ="1.- A usted le gusta las películas de terror?";
  pre2: string = "2.- Que color de bolígrafo usa?";
  pre3: string = "3.- De que color es el caballo blanco de Napoleon?";
  pre4: string = "4.- Que es más pesado, 1 kilo de plumas o 1 kilo de Oro?";
  pre5: string = "5.- Cuantos dedos tiene una gallina en su pata?";

  constructor(private fb: FormBuilder) { 
    this.crearCuestionario()
  }

  ngOnInit(): void {
  }

  get aceptarNoValido(){
    return this.cuestionario.get("aceptar")?.invalid && this.cuestionario.get("aceptar")?.touched
  }

  crearCuestionario():void{
    console.log("crearCues");
    this.cuestionario = this.fb.group({
      pregunta1:[""],
      pregunta2: [""],
      pregunta3: [""],
      pregunta4: [""],
      pregunta5: [""],
      aceptar:[""]
    })
  }

  guardar(): void{
    console.log(this.cuestionario.value);
    console.log(`${this.pre1}\nR.-${this.cuestionario.value.pregunta1}`);
    console.log(`${this.pre2}\nR.-${this.cuestionario.value.pregunta2}`);
    console.log(`${this.pre3}\nR.-${this.cuestionario.value.pregunta3}`);
    console.log(`${this.pre4}\nR.-${this.cuestionario.value.pregunta4}`);
    console.log(`${this.pre5}\nR.-${this.cuestionario.value.pregunta5}`);
    
  }
mostrar=false
  
}
